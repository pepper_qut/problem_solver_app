<?xml version="1.0" encoding="UTF-8" ?>
<Package name="SolverApp" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="README" src="README.md" />
        <File name="calendar" src="html/css/calendar.css" />
        <File name="style" src="html/css/style.css" />
        <File name="delete" src="html/images/delete.png" />
        <File name="edit" src="html/images/edit.png" />
        <File name="icon" src="html/images/icon.png" />
        <File name="index" src="html/index.html" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="badnames" src="html/js/badnames.js" />
        <File name="badwords" src="html/js/badwords.js" />
        <File name="pureJSCalendar" src="html/js/pureJSCalendar.js" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
