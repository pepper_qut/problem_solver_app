/* Create a session for the connection */
var session = new QiSession();

var fName = null;
var problemArea = null;
var problemDescriptions = [];
var chosenProblemDesc = null;
var ideaDescriptions = [];
var chosenIdeas = [];
var currentHint = null;

var waitingPage = null;
var focusNode = null;

//var randomfileName;

function note(str) {
	record = document.getElementById("debug").innerHTML;
	document.getElementById("debug").innerHTML = str +"<br>"+record;
}

function endSpeech() {
	if (waitingPage && waitingPage.style.display=='none') {
		waitingPage.style.display = 'flex';
		//anim.run("animations/Stand/Gestures/BodyTalk_15");
		focusNode.focus();
	} /*else {
		anim.run("animations/Stand/Gestures/BodyTalk_9");
	}*/
}

function startSubscribe(introStr) {
	session.service("ALMemory").done(function (ALMemory) {
        ALMemory.subscriber("ALAnimatedSpeech/EndOfAnimatedSpeech").done(function(subscriber) {
            subscriber.signal.connect(endSpeech);
        });   
		memory = ALMemory;
    });
	session.service('ALBehaviorManager').then(function (service) {
		behaviourManager = service;
		note("behaviourManager: "+behaviourManager);
	});
	session.service("ALTextToSpeech").done(function (service) {
		speechService = service;
		speechService.setParameter("speed",85);
    });
	session.service("ALAnimatedSpeech").done(function (service) {
		tts = service;
		tts.say(introStr);
    });
	session.service("ALTabletService").done(function (service) {
		tabletService = service;
    });
	session.service("ALBasicAwareness").done(function (service) {
		basicAwareness = service;
		// Stop Pepper looking down at tablet every time you touch it!
		basicAwareness.setStimulusDetectionEnabled("TabletTouch", false);
    });
	session.service("ALBackgroundMovement").done(function (service) {
		backgroundMovement = service;
		// Control background movement
		// backgroundMovement.setEnabled(false);
    });
}

function sayText(response) {
	tts.say(response);
	//tts.say("OK");
}

function interrupt_and_show(elementID) {
	speechService.stopAll();
	show(elementID);
}

function show(elementID) {
	// set background movement back on
	backgroundMovement.setEnabled(true);
	
    // try to find the requested page and alert if it's not found
    var ele = document.getElementById(elementID);
    if (!ele) {
        alert("no such page");
        return;
    }
	waitingPage = ele; // In case we have to wait for speech to finish

    // get all pages, loop through them and hide them
    var pages = document.getElementsByClassName('page');
	var pageCount = pages.length;
	var pageNum = 0;
    for(var i = 0; i < pageCount; i++) {
        pages[i].style.display = 'none';
		if (ele == pages[i]) {
			pageNum = i;
		}
    }
	var progressBar = document.getElementById("progress");
    if (progressBar) {
        //alert("Progress = "+parseInt(i)+" out of "+parsInt(pageCount));
		progressBar.style.visibility = "hidden";
		var progress = parseInt(pageNum*100/pageCount);
		styleStr = progress+"vw";
		//alert(styleStr);
		progressBar.style.width = styleStr;
		progressBar.style.visibility = "visible";
    }
				
	// find child elements
	var speaking = false;
	var children = ele.childNodes;
	for(var i = 0; i < children.length; i++) {
		var className = children[i].className;
        if (className == "aloud") {
			speaking = true;
			sayText(children[i].innerText);
		} else if (className == "userInput") {
			// Turn off the background movement, so it's easier to make input
			backgroundMovement.setEnabled(false);
			focusNode = children[i];
		} else if (className == "hint") {
			currentHint = children[i];
		}
    }
	
	// Finally show the requested page, unless waiting for speaking
    if (!speaking) {
		waitingPage.style.display = 'flex';
		focusNode.focus();
	}
}

function setName(nextPage) {
	var ele = document.getElementById("firstNameText");
        if (!ele) {
            alert("no element");
        } else {
			fName = ele.value;
		};
	if (fName.length == 0) {	
		recordData('firstName',fName);
		show(nextPage);
	} else if (dirtyWords(fName,badNames)) {
		fName = '';
		recordData('firstName',fName);
		show(nextPage);
	} else {
		recordData('firstName',fName);
		var nameTags = document.getElementsByClassName('firstName');
		for(var i = 0; i < nameTags.length; i++) {
			nameTags[i].innerText = fName;
		};
		show(nextPage);
	}
}

function dirtyWords(inputStr,badList) {
	//alert(inputStr);
	wordList = inputStr.split(' ');
	//alert(wordList);
	for (var index = 0; index < wordList.length; ++index) {
		word = wordList[index].toLowerCase();
		//alert(word)
		if (badList.indexOf(word) >= 0) {
			//alert("BAD WORD! "+badList[badList.indexOf(word)]);
			return true;
		}
	}
	return false;
}
	
function checkText(strInput,sayEmpty,sayDirty) {
	if (strInput.length == 0) {
		sayText(sayEmpty);
		return false;
	} else if (dirtyWords(strInput,badWords)) {
		sayText(sayDirty);
		return false;
	} else {
		return true;
	}
}

function showHint(set) {
	if (!currentHint) {
		alert("no current hint");
		return;
	}
	if (set) {
		currentHint.style.display='block';
	} else {
		currentHint.style.display='none';
	}
}

function setProblemArea(nextPage) {
	var checkedItem = null;
	problemArea = null;
	var options = document.getElementsByName("problemArea");
	for(var i=0; options[i]; ++i){
		if(options[i].checked){
			checkedItem = options[i].previousSibling;
			problemArea = checkedItem.textContent;
			recordData("problemArea",problemArea);
			break;
		}
	}
	if (problemArea == null) {
		sayText("Oops. Please select the topic that is closest to the area you would like to work on today.");
	} else {
		var ele = document.getElementsByClassName("problemArea");
			for(var i = 0; i < ele.length; i++) {
				ele[i].innerText = problemArea;
			}
		show(nextPage);
	}
}

function recordFeedback(name1,name2,nextPage) {
	var resp1 = -1;
	var options = document.getElementsByName(name1);
	for(var i=0; options[i]; ++i){
		if(options[i].checked){
			resp1=i;
			break;
		}
	}
	var resp2 = -1;
	options = document.getElementsByName(name2);
	for(var i=0; options[i]; ++i){
		if(options[i].checked){
			resp2=i;
			break;
		}
	}
	if (resp1 < 0 || resp2 <0) {
		sayText("Please answer both questions before we go on.");
	} else {
		recordData(name1,resp1.toString());
		recordData(name2,resp2.toString());
		show(nextPage);
	}
}

function chooseIdeas(nextPageID) {
	var checkedValue = null;
	options = document.getElementsByName("ideaDesc");
	for(var i=0; options[i]; ++i){
		if(options[i].checked){
			//checkedValue = i;
			//alert(options[i].value);
			chosenIdeas.push(options[i].value);
		}
	}
	var ideaCount = chosenIdeas.length;
	if (ideaCount == 0) {
		sayText("Oops. Please select at least one idea to work with.");
	} else if (ideaCount > 3) {
		sayText("Oops. Let's just stick with three ideas to work with for now. Please select up to three, and hit Next when you're ready.");
		chosenIdeas = [];
	} else {
		if (ideaCount > 1) {
			//sayText("Good. You have chosen "+ideaCount+" ideas to help solve your problem:");
			sayText("Your chosen ideas are: \\pau=500\\");
		} else {
			//sayText("Good. The idea you have chosen to work with is:");
			sayText("Your chosen idea is: \\pau=500\\");
		}
		for(var i=0; i<3; i++){
			if (i < ideaCount) {
				sayText(chosenIdeas[i]+"\\pau=500\\");
				recordData("chosenIdea",chosenIdeas[i]);
			} else {
				recordData("chosenIdea","");
			}
		}
		show(nextPageID);
	}
}

function listProblems(nextPage) {
	sayText("Your topic statements are: ");
	var plist = document.getElementById("problemList");
        if (!plist) {
            alert("no element");
			sayText("You're not the only one with problems");
		};
	var problemDescElems = document.getElementsByName("problem_area");
		for(var i = 0; i < problemDescElems.length; i++) {
			var prob = problemDescElems[i].value;
			recordData("problemStatement",prob);
            if (prob.length>0) {
				sayText("\\pau=600\\"+prob);
				problemDescriptions.push(prob);
				var lab = document.createElement("label");
				lab.className = "container";
				var p = document.createTextNode(prob);
				lab.appendChild(p);
				var inp = document.createElement("input");
				inp.type = "radio";
				inp.name = "problemDesc";
				//inp.value = i;
				lab.appendChild(inp);
				var s = document.createElement("span")
				s.className = "checkmark";
				lab.appendChild(s);
				plist.appendChild(lab);
			};
        };
	show(nextPage);
}

function chooseProblem(nextPageID) {
	//var checkedValue = null;
	var options = document.getElementsByName("problemDesc");
	for(var i=0; i<options.length; ++i){
		if (options[i].checked) {
			//checkedValue = i;
			//chosenProblemDesc = problems[i].value;
			chosenProblemDesc = options[i].previousSibling.textContent;
			recordData("chosenProblem",chosenProblemDesc);
			break;
		}
	}
	if (!chosenProblemDesc) {
		sayText("Oops. Please choose an issue before we go on.");
		sayText("Tap Next when you're ready.");
		return;
	}
	sayText("Alright. Let's get started on your issue: ");
	sayText(chosenProblemDesc + "\\pau=500\\");
	var ele = document.getElementsByClassName("chosenProblem");
		for(var i = 0; i < ele.length; i++) {
            ele[i].innerText = chosenProblemDesc;
        }
	show(nextPageID);
}

function listIdeas() {
	var plist = document.getElementById("ideaListEdit");
        if (!plist) {
            alert("no element");
		};
	sayText("Think about things you like about each idea you have suggested."); 
	ideas = document.getElementsByName("Idea");
		for(var i = 0; i < ideas.length; i++) {
			var prob = ideas[i].value;
			recordData("idea",prob);
            if (prob.length>0) {
				//sayText(prob);
				var lab = document.createElement("label");
				lab.className = "leftrow";
				lab.setAttribute("name","EditedIdea");
				var delButton = document.createElement("button");
				delButton.className = "deleteButton";
				delButton.onclick = function () {
					//var textStr = this.parentNode.innerText;
					//alert("Delete: "+textStr);
					popupDelete(this.parentNode);
				};
				lab.appendChild(delButton);
				var editButton = document.createElement("button");
				editButton.className = "editButton";				
				editButton.onclick = function () {
					var textStr = this.parentNode.innerText;
					//alert("Edit: "+textStr);
					popupEdit(this.parentNode);
				};
				lab.appendChild(editButton);
				//var p = document.createTextNode(prob);
				var p = document.createElement("label");
				p.innerHTML = prob;
				lab.appendChild(p);
				plist.appendChild(lab);
			};
        };
}
function popupEdit(node) {
	//alert(node.innerText);
	var popupWindow = document.getElementById("editPopup");
	var txtArea = document.getElementById("editedText");
	txtArea.value = node.lastChild.innerText;
	var saveButton = document.getElementById("popupSave");
	//saveButton.innerText = "SAVE"; 
	saveButton.onclick = function () {
		if (dirtyWords(txtArea.value,badWords)) {
			sayText("Um. Can you say that another way?");
		} else {
			node.lastChild.innerText = txtArea.value;
			//alert("saving "+node.innerText);
			popupWindow.style.display='none';
		};
	}; 
	popupWindow.style.display = 'block';
	txtArea.focus();
}
function popupDelete(node) {
	//alert("deleting "+node.innerText);
	var popupWindow = document.getElementById("deletePopup");
	var txtArea = document.getElementById("deleteText");
	txtArea.innerHTML = node.lastChild.innerText; 
	var saveButton = document.getElementById("popupDelete");
	//saveButton.innerText = "DELETE"; 
	saveButton.onclick = function () {
		node.parentNode.removeChild(node);
		popupWindow.style.display='none';
	}; 
	popupWindow.style.display = 'block';
}

function listRefinedIdeas() {
	var plist = document.getElementById("ideaList");
        if (!plist) {
            alert("no element");
		};
	var ideas = document.getElementsByName("EditedIdea");
	//alert(ideas);
	//alert(ideas.length);
	if (ideas.length ==0) {
		var ideaFields = document.getElementsByName("Idea");
		for(var i = 0; i < ideaFields.length; i++) {
			ideaFields[i].value = "";
		}
		return false;
	} else {
		for(var i = 0; i < 5; i++) {
			if (i < ideas.length) {
				var prob = ideas[i].innerText;
				recordData("refinedIdea",prob);
				if (prob.length>0) {
					//sayText(prob);				
					var lab = document.createElement("label");
					lab.className = "container";
					var p = document.createTextNode(prob);
					lab.appendChild(p);
					var inp = document.createElement("input");
					inp.type = "checkbox";
					inp.name = "ideaDesc";
					inp.value = prob;
					lab.appendChild(inp);
					var s = document.createElement("span")
					s.className = "checkmark2";
					lab.appendChild(s);
					plist.appendChild(lab);
				};
			} else {
				recordData("refinedIdea","");
			};
        };
		return true;
	}
}

function hint(hintStr) {
	alert(hintStr);
}

function addAnother(answerID,nextPageID,skipPageID) {
	var answerStr = "";
	var ele = document.getElementById(answerID);
        if (!ele) {
            alert("no element");
        } else {
			answerStr = ele.value;
		};
	if (answerStr.length == 0) {
		//sayText("That's OK.");
		listProblems();
		show(skipPageID)
	} else {
		//sayText("OK.");
		if (nextPageID == skipPageID) {
			listProblems();
		}
		show(nextPageID);
	};
}

function addAnotherIdea(answerID,nextPageID,skipPageID) {
	var answerStr = "";
	var ele = document.getElementById(answerID);
        if (!ele) {
            alert("no element");
        } else {
			answerStr = ele.value;
		};
	if (answerStr.length == 0) {
		//sayText("That's OK.");
		listIdeas();
		show(skipPageID)
	} else if (dirtyWords(answerStr,badWords)) {
		sayText("Um. Can you say that another way?");
	} else {
		if (nextPageID == skipPageID) {
			listIdeas();
		}
		show(nextPageID);
	};
}

function stopApp() {
	pathName = window.location.pathname;
	path = pathName.slice(0,pathName.lastIndexOf('/'));
	behaviourName = path.replace("/apps/", "")+"/behavior_1";
	behaviourManager.stopBehavior(behaviourName);
}

function confirmPlanInput(tryString, dateString, nextPage) {
	if (tryString.length == 0) {
		sayText('Please enter something before we go on. Tap the blue button for a hint if you need help.');
	} else if (dirtyWords(tryString,badWords)) {
		sayText('Um. Can you say that another way?');
	} else if (dateString == 'dd.mm.yyyy') {
		sayText('Oops. Please select a date first');
	} else {
		show(nextPage);
	}
}

function setPlan(nextPage) {
	//alert("setting plan");
	//alert("fName = "+fName);
	var newStr;
	if (fName) {
		//alert(fName);
		var planName = fName+"'s Plan";
		//alert(planName);
		document.getElementById("planHeader").innerHTML = planName;
	};
	var elList = document.getElementById('finalIdeaList');
	for (i = 0; i < chosenIdeas.length; i++) { 
		var node = document.createElement("li");
		var textnode = document.createTextNode(chosenIdeas[i]);         
		node.appendChild(textnode);     
		elList.appendChild(node);
	}
	var eleHow = document.getElementById("planHow");
	howStr = document.getElementById("How").value;
	eleHow.innerHTML += howStr;
	recordData("How",howStr);
	var eleWhen = document.getElementById("planWhen");
	whenStr = document.getElementById("startDate").innerHTML;
	eleWhen.innerHTML += whenStr;
	recordData("startDate",whenStr);
	var eleCheck = document.getElementById("planCheck");
	checkStr = document.getElementById("checkDate").innerHTML;
	eleCheck.innerHTML += checkStr;
	recordData("checkDate",checkStr);
	show(nextPage);
}

function checkEmail(nextPage) {
	var emailAddr = document.getElementById("emailTxt").value;
	var sendPlan = document.getElementById("sendPlanCheck").checked;
	//alert(sendPlan);
	if (emailAddr.length == 0) {
		if (sendPlan) {
			sayText("Oops. I will need your email address to send you a copy of your plan.");
		} else {
			sayText("Thats OK");
			show(nextPage);
		}
	} else {
		var ea = emailAddr.trim();
		if (ea.indexOf('@') == -1 || ea.indexOf('.') == -1) {
			sayText("Oh dear. That doesn't look right. Please check your email address and try again.");
		} else {
			recordData("sendPlan", sendPlan.toString());
			recordData("emailAddress",ea);
			show(nextPage);			
		}
	}
}

function startLog() {
	randomfileName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	//alert(randomfileName);
	memory.raiseEvent("startLog", randomfileName);
}
function recordData(nameStr,valStr) {
	if (nameStr == "emailAddress") {
		//valStr = Tea.encrypt(valStr, randomfileName);
		//alert("email encrypted: "+valStr);
		memory.raiseEvent("stopLog", valStr.trim());
		return;
	}
	var rec = '"'+valStr.trim()+'"';
	memory.raiseEvent("writeData", rec);
}